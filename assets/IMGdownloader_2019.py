import os
import aiohttp
import asyncio
import uuid
from bs4 import BeautifulSoup

IMAGE_DIR = 'TEGBR/image/2019'
os.makedirs(IMAGE_DIR, exist_ok=True)

MAX_RETRIES = 5
RETRY_DELAY = 3

async def download_image(session, url, filename):
    for attempt in range(1, MAX_RETRIES + 1):
        try:
            async with session.get(url, timeout=6000) as response:
                if response.status == 200:
                    with open(filename, 'wb') as f:
                        f.write(await response.read())
                    print(f'Downloaded {url} to {filename}')
                    return
                else:
                    print(f'Failed to download {url}: {response.status}')
        except (aiohttp.ClientError, aiohttp.ServerTimeoutError, asyncio.TimeoutError) as e:
            print(f'Error downloading {url}: {e}')
        
        if attempt < MAX_RETRIES:
            wait_time = RETRY_DELAY * (2 ** (attempt - 1))
            print(f'Retrying {url} in {wait_time} seconds (Attempt {attempt + 1}/{MAX_RETRIES})')
            await asyncio.sleep(wait_time)

    print(f'Giving up on {url} after {MAX_RETRIES} attempts')

async def process_html(html_path):
    with open(html_path, 'r', encoding='utf-8') as f:
        html_content = f.read()

    soup = BeautifulSoup(html_content, 'html.parser')
    img_tags = soup.find_all('img')

    download_tasks = []
    async with aiohttp.ClientSession() as session:
        for img_tag in img_tags:
            src = img_tag.get('src')
            if src:
                unique_filename = f'{IMAGE_DIR}/{uuid.uuid4()}.jpg'
                download_tasks.append(download_image(session, src, unique_filename))
                img_tag['src'] = unique_filename

        await asyncio.gather(*download_tasks)

    new_html_path = 'modified_' + os.path.basename(html_path)
    with open(new_html_path, 'w', encoding='utf-8') as f:
        f.write(str(soup))
    
    print(f'Modified HTML saved to {new_html_path}')

async def main():
    html_path = 'TEGBR_2019.html'
    await process_html(html_path)

if __name__ == '__main__':
    asyncio.run(main())
